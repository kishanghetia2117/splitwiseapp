// 1
// create components - > redux -> (feature)/cake folder

// 2 define types
// create (feature)Types.js
export const BUY_CAKE = 'BUY_CAKE'

// 3 define action 
// create (feature)Action.js
// import type from types 
// export an action (= () => function)
// return an object with type inside the funtion 
import { BUY_CAKE } from './cakeTypes'
export const buyCake = () => {
  return {
    type: BUY_CAKE
  }
}


//4 define reducer 
import { BUY_CAKE } from './cakeTypes'
// give initial state
const initialState = {
  numOfCakes: 10,
}
// default state = initialState and action
// use switch (action.type to find the action type) 
// retunr current state and change in state 
const cakeReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_CAKE: return {
      ...state,
      numOfCakes: state.numOfCakes - 1
    }
    default: return state
  }
}
export default cakeReducer

//5 define store 
// create store.js under redux
import { createStore } from 'redux'
import cakeReducer from './cake/cakeReducer'
const store = createStore(cakeReducer);
export default store;

// 6 pass redux to react components
import './App.css';
import { Provider } from 'react-redux'; // import provider
import store from './redux/store'
import CakeContainer from './components/CakeContainer'
function App() {
  return (
    <Provider store={store}> {/* // pass store as props */}
      <div className="App">
        <CakeContainer />
      </div>
    </Provider>
  );
}
export default App;

// 7 create index.js inside redux components
export { buyCake } from './cake/cakeaction'

// 8 dispatch an action , get hold of a state
import React from 'react';
import { connect } from 'react-redux'
import { buyCake } from '../redux'
// 0
function CakeContainer(props) {
  return <div>
    <h2>Number of cakes - {props.numOfCakes}</h2>
    <button onClick={props.buyCake}>Buy Cake</button>
  </div>;
}

const mapStateToProps = (state) => {
  return {
    numOfCakes: state.numOfCakes
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    buyCake: () => dispatch(buyCake())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer);