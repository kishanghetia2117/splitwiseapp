import './App.css';

import { Routes, Route, Link } from "react-router-dom";
import LandingPage from './compoents/LandingPage';
import Signup from './compoents/Signup';
import Login from './compoents/Login'


function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </div>
  );
}

export default App;
