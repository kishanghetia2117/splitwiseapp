import React, { Component } from 'react';

import Header from './landingComponents/Header'
import Landing from './landingComponents/Landing'
import Demo from './landingComponents/Demo'
import ProDemo from './landingComponents/ProDemo'
import Features from './landingComponents/Features'
import Reviews from './landingComponents/Reviews'
import Footer from './landingComponents/Footer'

class LandingPage extends Component {
  render() {
    return <div className="bg-white">
      <Header />
      <Landing />
      <Demo />
      <ProDemo />
      <Features />
      <Reviews />
      <Footer />
    </div>;
  }
}

export default LandingPage;




// return <div className="mt-2">
// <Link to="/login" className='p-1'>
//   <button type="button" className="btn btn-outline-success me-2">Login</button>
// </Link>
// <Link to="/signup" className='p-1'>
//   <button type="button" className="btn btn-success">Sign up</button>
// </Link>
// </div>;