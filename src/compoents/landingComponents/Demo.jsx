import React, { Component } from 'react';
import Asset1 from '../image/asset1.png'
import Asset2 from '../image/asset2.png'
import Asset3 from '../image/asset3.png'
import Asset4 from '../image/asset4.png'

class Demo extends Component {
  render() {
    return <div className="constainer text-white">
      <div className="row gutter-zero">
        <div className="col-sm-6 align-content-between bg-abstract fill-grey ">
          <div className="p-4">
            <h4 className='mt-3 p-2'> Track balances</h4>
            <p className='w-50 px-5 mx-auto'> Keep track of shared expenses, balances, and who owes who.</p>
          </div>
          <div className="">
            <img className="mobile-bottom" src={Asset1} alt="mobile" />
          </div>
        </div>
        <div className="col-sm-6 align-content-start bg-abstract fill-green ">
          <div className="p-4">
            <h4 className='mt-3 p-2'>Organize expenses</h4>
            <p className='w-50 px-5 mx-auto'> Split expenses with any group: trips, housemates, friends, and family.</p>
          </div>
          <div className="">
            <img className="mobile-bottom" src={Asset2} alt="mobile f" />
          </div>
        </div>
        <div className="col-sm-6 align-content-start bg-abstract fill-orange ">
          <div className="p-4">
            <h4 className='mt-3 p-2'>Add expenses easily</h4>
            <p className='w-50 px-5 mx-auto'> Quickly add expenses on the go before you forget who paid.</p>
          </div>
          <div className="">
            <img className="mobile-bottom" src={Asset3} alt="mobile" />
          </div>
        </div>
        <div className="col-sm-6 align-content-start bg-abstract fill-grey ">
          <div className="p-4">
            <h4 className='mt-3 p-2'>Pay friends back</h4>
            <p className='w-50 px-5 mx-auto'>Settle up with a friend and record any cash or online payment.</p>
          </div>
          <div className="">
            <img className="mobile-bottom" src={Asset4} alt="mobile" />
          </div>
        </div>
      </div>
    </div>;
  }
}

export default Demo;
