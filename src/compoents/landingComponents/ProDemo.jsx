import React, { Component } from 'react';
import Asset5 from '../image/asset5.png'


class ProDemo extends Component {
  render() {
    return <div className="container-fluid bg-abstract fill-purple text-white">
      <div className="row justify-content-center align-items-center ">
        <div className="col-sm-6 justify-content-center align-items-center">
          <div className="d-flex flex-column justify-content-center align-items-center">
            <h4 className=''> Get even more with PRO</h4>
            <p className="w-50 px-5 mx-auto">
              Get even more organized with receipt scanning, charts and graphs, currency conversion, and more!
            </p>
            <button type="button" className="btn btn-outline-light btn-lg">Sign up</button>
          </div>
        </div>
        <div className="col-sm-6 d-flex justify-content-center align-item-center mt-5 ">
          <div className="pt-5">
            <img src={Asset5} alt="asset-5" />
          </div>
        </div>
      </div>
    </div>
  }
}

export default ProDemo;
