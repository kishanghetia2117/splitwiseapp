import React, { Component } from 'react';
import Icon1 from '../icons/core-feature.svg';
import Icon2 from '../icons/pro-feature.svg'

class Features extends Component {
  render() {
    return <div className='container mx-auto mt-5'>
      <div>
        <h1 className='h1 text-center p-5 mt-5 fw-bold'>The whole nine yards</h1>
      </div>
      <div className='row p-3'>
        <div className='col-sm-4'>
          <ul className='seprator'>
            <li>
              <img src={Icon1} alt="" /> <span>Add groups and friends</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Split expenses, record debts</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Equal or unequal splits</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Split by % or shares</span>
            </li>

            <li>
              <img src={Icon1} alt="" /> <span>Calculate total balances</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Suggested repayments</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Simplify debts</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Recurring expenses</span>
            </li>
          </ul>
        </div>

        <div className='col-sm-4'>
          <ul className='seprator'>


            <li >
              <img src={Icon1} alt="" /> <span>Offline mode</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Cloud sync</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Spending totals</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Categorize expenses</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>
                Easy CSV exports</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>7+ languages</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>100+ currencies</span>
            </li>
            <li>
              <img src={Icon1} alt="" /> <span>Payment integrations</span>
            </li>
          </ul>
        </div>
        <div className='col-sm-4'>
          <ul className='seprator'>
            <li>
              <img src={Icon2} alt="" /> <span>A totally ad-free experience</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>
                Currency conversion</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>Receipt scanning</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>Itemization</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>Charts and graphs</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>
                Expense search</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>
                Save default splits</span>
            </li>
            <li>
              <img src={Icon2} alt="" /> <span>Early access to new features</span>
            </li>
          </ul>

        </div>

      </div>

    </div>;
  }
}

export default Features;
