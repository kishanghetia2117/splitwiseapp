import React, { Component } from 'react';

class Reviews extends Component {
  render() {
    return <div className="container mb-5">
      <div className="row">
        <div className="col-sm-4 ">
          <div className="card text-start card-size">
            <div className="card-body">
              <h5 className="fs-5 fw-light card-text">“Fundamental” for tracking finances. As good as WhatsApp for containing awkwardness.</h5>
            </div>
            <div className="card-footer">
              Financial Times
            </div>
          </div>
        </div>
        <div className="col-sm-4 ">
          <div className="card text-start card-size">
            <div className="card-body">
              <h5 className="fs-5 fw-light card-text">Makes it easy to split everything from your dinner bill to rent.</h5>
            </div>
            <div className="card-footer">
              Ahah S, iOS
            </div>
          </div>
        </div>
        <div className="col-sm-4 ">
          <div className="card text-start card-size">
            <div className="card-body">
              <h5 className="fs-5 fw-light card-text">“So amazing to have this app manage balances and help keep money out of relationships. love it!</h5>
            </div>
            <div className="card-footer">
              NY Times
            </div>
          </div>
        </div>
        <div className="col-sm-4 ">
          <div className="card text-start card-size">
            <div className="card-body">
              <h5 className="fs-5 fw-light card-text">I never fight with roommates over bills because of this genius expense-splitting app</h5>
            </div>
            <div className="card-footer">
              Haseena C, Android
            </div>
          </div>
        </div>
        <div className="col-sm-4 ">
          <div className="card text-start card-size">
            <div className="card-body">
              <h5 className="fs-5 fw-light card-text">I use it everyday. I use it for trips, roommates, loans. I love splitwise.</h5>
            </div>
            <div className="card-footer">
              Business Insider
            </div>
          </div>
        </div>
        <div className="col-sm-4 ">
          <div className="card text-start card-size">
            <div className="card-body">
              <h5 className="fs-5 fw-light card-text">So amazing to have this app manage balances and help keep money out of relationships. love it!</h5>
            </div>
            <div className="card-footer">
              Trickseyus, iOS
            </div>
          </div>
        </div>
      </div>
    </div>;
  }
}

export default Reviews;
