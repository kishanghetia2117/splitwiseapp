import React, { Component } from 'react';


class Debit extends Component {
  render() {
    const { data } = this.props;
    return <div className="w-100 d-flex p-1">
      <div className="w-25">
        <img className="rounded-circle w-100 p-2" src="https://s3.amazonaws.com/splitwise/uploads/user/default_avatars/avatar-blue37-100px.png" alt="" />
      </div>
      <div className="flex-grow-1">
        <h4 className="mb-0 mt-2">{data.friendName}</h4>
        <p className="mb-0">{'You own $' + data.friendShare}</p>
      </div>
    </div>
  }
}

export default Debit;
