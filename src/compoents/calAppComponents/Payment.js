import React, { Component } from 'react';

class Payment extends Component {
  render() {
    const { expData, userData } = this.props;
    let date = new Date(expData.date)
    const monthNames = ["Jan", "Feb", "Mar", "Ap", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    let month = monthNames[date.getMonth()]
    date = date.getDate()
    date = ("0" + date).slice(-2);
    console.log(date)
    return <div>{expData.userPaid ?
      <div className="d-flex justify-content-between align-items-center m-3">
        <div className="d-flex tex-center align-items-center w-50">
          <div className="date me-2  w-25" title="2022-02-02T09:21:55Z">{month} <div className="number">{date}</div></div>
          <img className="w-20per mx-2" src="https://s3.amazonaws.com/splitwise/uploads/category/icon/square_v2/uncategorized/general@2x.png" alt="" />
          <p className="description mx-2 my-0 text-start fw-bold">
            {expData.description}
          </p>
        </div>
        <div className="d-flex tex-center align-items-center justify-content-evenly w-50">
          <div className="d-flex flex-wrap tex-center align-items-center justify-content-end w-50 text-end me-2">
            <p className="w-100 my-0 text-muted">You paid</p>
            <p className="w-100 my-0">{expData.totalAmt}</p>
          </div>
          <div className="d-flex flex-wrap tex-center align-items-center w-50 justify-content-start text-start ms-2">
            <p className="w-100 my-0 text-muted">{"You lent " + expData.friendName}</p>
            <p className="w-100 my-0 text-green">{expData.friendShare}</p>
          </div>
        </div>
      </div>
      :
      <div className="d-flex justify-content-between align-items-center m-3">
        <div className="d-flex tex-center align-items-center w-50">
          <div className="date me-2  w-25" title="2022-02-02T09:21:55Z">{month} <div className="number">{date}</div></div>
          <img className="w-20per mx-2" src="https://s3.amazonaws.com/splitwise/uploads/category/icon/square_v2/uncategorized/general@2x.png" alt="" />
          <p className="description mx-2 my-0 text-start fw-bold">
            {expData.description}
          </p>
        </div>
        <div className="d-flex tex-center align-items-center justify-content-evenly w-50">
          <div className="d-flex flex-wrap tex-center align-items-center justify-content-end w-50 text-end me-2">
            <p className="w-100 my-0 text-muted">{expData.friendName + " paid"}</p>
            <p className="w-100 my-0">{expData.totalAmt}</p>
          </div>
          <div className="d-flex flex-wrap tex-center align-items-center w-50 justify-content-start text-start ms-2">
            <p className="w-100 my-0 text-muted">{expData.friendName + " lent you"} </p>
            <p className="w-100 my-0 text-orange">{expData.friendShare}</p>
          </div>
        </div>
      </div>
    }</div>
  }
}

export default Payment;
