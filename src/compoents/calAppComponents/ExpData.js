import React, { Component } from 'react';
import DashBord from './Dashboard'
import { connect } from 'react-redux'
import AddExp from './AddExp'
import SettleExp from './SettleExp'
import SubSection from './SubSection'

const mapStateToProps = state => {
  return {
    expRecord: state.expRecord,
  }
}
class expData extends Component {
  render() {
    const { expRecord } = this.props
    let getActiveCredAndDeb = expRecord.reduce((active, item) => {
      if (active.cred === undefined) {
        active['cred'] = 0
        active['deb'] = 0
      }
      if (item.settled === false && item.userPaid === false && item.typeSettleUp === false) {
        active['deb'] += item.userShare
      }
      if (item.settled === false && item.userPaid === true && item.typeSettleUp === false) {
        active['cred'] += item.friendShare
      }
      return active
    }, {})
    let totalBalance = getActiveCredAndDeb.cred - getActiveCredAndDeb.deb
    const { dahsProp } = this.props
    return <div className="col-sm-7 vh-100 dashBord-con p-0">
      <div className="log bg-light p-1">
        <div className="d-flex justify-content-between p-3 border-bottom">
          {dahsProp ? <h3 className="">DashBord</h3> : <h3>All expenses</h3>}
          <div className="d-flex">
            <AddExp />
            <SettleExp />
          </div>
        </div>
        <div className="d-flex bg-light border-bottom">
          <div className="flex-grow-1">
            <p className="mb-1">total balance</p>
            <p className="">{'$' + totalBalance}</p>
          </div>
          <div className="flex-grow-1">
            <p className="mb-1">you owe</p>
            <p className="">{'$' + getActiveCredAndDeb.deb}</p>
          </div>
          <div className="flex-grow-1">
            <p className="mb-1">you are owed</p>
            <p className="">{'$' + getActiveCredAndDeb.cred}</p>
          </div>
        </div>
      </div>
      {dahsProp ?
        <DashBord /> :
        <SubSection />
      }
    </div>
  }
}

export default connect(mapStateToProps)(expData);
