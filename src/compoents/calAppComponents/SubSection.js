import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";
import { connect } from 'react-redux'
import SettledCard from './SettledCard'
import Payment from './Payment'
const mapStateToProps = state => {
  return {
    userData: state.userData,
    expRecord: state.expRecord,
    friends: state.friends,
  }
}

class SubSection extends Component {

  render() {
    const { expRecord, userData } = this.props
    return <div>
      {expRecord.map(item => {
        return item.typeSettleUp == true ?
          <SettledCard key={item.expId} expData={item} userData={userData} />
          :
          <Payment key={item.expId} expData={item} userData={userData} />
      })
      }
    </div>
  }
}

export default connect(mapStateToProps)(SubSection);
