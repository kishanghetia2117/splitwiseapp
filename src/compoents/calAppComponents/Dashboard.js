import React, { Component } from 'react';
import Credits from './Credits'
import Debit from './Debit'
import { connect } from 'react-redux'

const mapStateToProps = state => {
  return {
    expRecord: state.expRecord,
  }
}

class Dashboard extends Component {
  render() {
    const { expRecord } = this.props
    let debtArr = expRecord.filter(item => {
      if (item.settled === false && item.userPaid === false && item.typeSettleUp === false) {
        return true
      }
      return false
    })
    let CredArr = expRecord.filter(item => {
      if (item.settled === false && item.userPaid === true && item.typeSettleUp === false) {
        return true
      }
      return false
    })
    return <div className="dashBord d-flex text-start">
      <div className=" w-50 border-end">
        {debtArr.map(item => <Debit key={item.expId} data={item} />)}
      </div>
      <div className=" w-50 ">
        {CredArr.map(item => <Credits key={item.expId} data={item} />)}
      </div>
    </div>
  }
}

export default connect(mapStateToProps)(Dashboard);
