import React, { Component } from 'react';
import { connect } from 'react-redux'

const mapStateToProps = state => {
  return {
    expRecord: state.expRecord,
  }
}

class ExpSummary extends Component {
  constructor(props) {
    super(props)

    this.state = {
      totalPaidFor: 0,
      totalShare: 0,
      paymentMade: 0,
      paymentRecived: 0,
      totalBalance: 0
    }
  }

  render() {
    const { expRecord } = this.props
    let calSummary = expRecord.reduce((summary, item) => {
      if (summary.tPaidFor === undefined) {
        summary['tPaidFor'] = 0
        summary['tShare'] = 0
        summary['pMade'] = 0
        summary['pRecived'] = 0
      }
      if (item.userPaid) {
        summary.tPaidFor += parseFloat(item.totalAmt)
      }
      summary.tShare += parseFloat(item.userShare)
      if (item.settled && item.userPaid === false) {
        summary.pMade += parseFloat(item.userShare)
      }
      if (item.settled && item.userPaid || item.typeSettleUp === true) {
        summary.pRecived += parseFloat(item.friendShare)
      }
      return summary
    }, {})
    let TotalChange = (calSummary.tPaidFor + calSummary.pMade) - calSummary.tShare - calSummary.pRecived
    return <div className="col-sm-3 mt-3">
      <div className="card">
        <ul className="list-group list-group-flush text-start">
          <li className="list-group-item border-0 fs-6">Total you paid for<br /><span className="fs-5 text-green">{'$' + calSummary.tPaidFor}</span></li>
          <li className="list-group-item border-0 fs-6">Your total share<br /><span className="fs-5 text-orange">{'$' + calSummary.tShare}</span></li>
          <li className="list-group-item border-0 fs-6">Payments made<br /><span className="fs-5 text-green">{'$' + calSummary.pMade}</span></li>
          <li className="list-group-item border-0 border-bottom fs-6">Payments received<br /><span className="fs-5 text-orange">{'$' + calSummary.pRecived}</span></li>
          <li className="list-group-item border-0 fs-6">Total change in balance<br /><span className="fs-5 text-green">{'$' + TotalChange}</span></li>
        </ul>
      </div>
    </div>
  }
}

export default connect(mapStateToProps)(ExpSummary);
