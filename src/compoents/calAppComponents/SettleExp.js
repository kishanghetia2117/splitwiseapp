import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import { updateExpRecord } from "../../redux"

import "react-datepicker/dist/react-datepicker.css";

import { connect } from 'react-redux'
const mapStateToProps = state => {
  return {
    expRecord: state.expRecord,
    friends: state.friends,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    updateExpRecord: (data = false) => dispatch(updateExpRecord(data)),
  }
}

class SettleExp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      amt: '',
      date: '',
      paidby: '0',
      paidTo: '1',
    }
  }
  handleListOne = (e) => {
    this.setState({ paidby: e.target.value });
  }
  handleListTwo = (e) => {
    this.setState({ paidTo: e.target.value });
  }
  handleAmt = (e) => {
    this.setState({ amt: e.target.value });
  }
  handleChange = (e) => {
    this.setState({ date: e });
  }
  handleSettleUp = (e) => {
    let onlydate = this.state.date.getDate();
    let month = this.state.date.getMonth() + 1;
    let year = this.state.date.getFullYear();

    let tempExpId = this.props.expRecord.length + 1;
    let tempDate = onlydate + '-' + month + '-' + year
    let tempTotalAmt = this.state.amt
    let tempUserPaid = this.state.paidby == 0 ? true : false
    let tempUserShare = this.state.paidby == 0 ? this.state.amt : 0
    let tempFriendShare = this.state.paidTo > 0 ? this.state.amt : 0
    let tempFriendName = this.props.friends.filter(item => item.Fid == this.state.paidTo).Fid

    let tempObj = {
      expId: tempExpId,
      date: tempDate,
      totalAmt: tempTotalAmt,
      userPaid: tempUserPaid,
      settled: true,
      userShare: tempUserShare,
      friendShare: tempFriendShare,
      friendName: tempFriendName,
      typeSettleUp: true
    }
    let newExpRecond = [...this.props.expRecord, tempObj]
    this.props.updateExpRecord(newExpRecond)
    this.setState({
      amt: '',
      date: '',
      paidby: '0',
      paidTo: '1',
    });
  }
  render() {
    const { amt, date, paidby, paidTo } = this.state
    console.log(this.state)
    const { friends } = this.props
    return <div>
      <button type="button" className="btn btn-success btn-primary-dark me-2" data-bs-toggle="modal" data-bs-target="#settle">Settle up</button>
      <div className="modal fade" id="settle" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header fill-green">
              <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <div className="images mb-4">
                <img className="rounded-circle w-10per" src="https://s3.amazonaws.com/splitwise/uploads/user/default_avatars/avatar-grey22-100px.png" alt="" />
                <img className="arrow mx-4" src="https://assets.splitwise.com/assets/fat_rabbit/settle-up-arrow-83553d33b6848bbdfa3499d7e217748aab1f75ff2073ec5ac67cba5246e12459.png" alt="" />
                <img className="rounded-circle w-10per" src="https://s3.amazonaws.com/splitwise/uploads/user/default_avatars/avatar-blue37-100px.png" alt="" />
              </div>
              <div className="d-flex mx-auto">
                <select className="form-select form-select-sm" onChange={this.handleListOne} value={this.state.paidby}>
                  {friends.map((item, index) => index === 0 ? <option key={item.Fid} value={item.Fid}>{item.FName}</option> : <option key={item.Fid} value={item.Fid}>{item.FName}</option>)}
                </select>
                <p className="px-4">paid</p>
                <select className="form-select form-select-sm" onChange={this.handleListTwo} value={this.state.paidTo}>
                  {friends.map((item, index) => index === 1 ? <option key={item.Fid} value={item.Fid}>{item.FName}</option> : <option key={item.Fid} value={item.Fid}>{item.FName}</option>)}

                </select>
              </div>
              <div className="input-group my-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">$</span>
                </div>
                <input type="text" className="form-control" aria-label="Amount (to the nearest dollar)" onInput={this.handleAmt} />
                <div className="input-group-append">
                  <span className="input-group-text">.00</span>
                </div>
              </div>
              <DatePicker selected={this.state.date} dateFormat="dd-MM-yyyy" onChange={this.handleChange} />
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary btn-primary-dark" data-bs-dismiss="modal" onClick={this.handleSettleUp}>Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettleExp);
