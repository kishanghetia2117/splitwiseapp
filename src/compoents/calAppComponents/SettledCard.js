
import React, { Component } from 'react';

class SettledCard extends Component {
  render() {
    const { expData, userData } = this.props;
    return <div className="text-green-card p-2">{expData.userPaid ?
      <div className="d-flex justify-content-between align-items-center ms-4">
        <div className="d-flex align-items-center w-50">
          <img className="w-10per" src="https://assets.splitwise.com/assets/api/payment_icon/square/small/offline.png" alt="" />
          <p className="mb-0 mx-3 fw-bold">{userData.Name + " paid " + expData.friendName + " $" + expData.totalAmt}</p>
        </div>
        <div className="d-flex w-50 justify-content-between align-items-center">
          <div className="text-start text-orange ">You paid</div>
          <div className="text-orange me-3">{'$' + expData.totalAmt}</div>
        </div>
      </div>
      :
      <div className="d-flex justify-content-between align-items-center ms-4">
        <div className="d-flex align-items-center w-50">
          <img className="w-10per" src="https://assets.splitwise.com/assets/api/payment_icon/square/small/offline.png" alt="" />
          <p className="mb-0 mx-3 fw-bold">{expData.friendName + " paid " + userData.Name + " $" + expData.totalAmt}</p>
        </div>
        <div className="d-flex w-50 justify-content-between align-items-center">
          <div className="text-start text-green" >You Recived</div>
          <div className="text-green me-3">{'$' + expData.totalAmt}</div>
        </div>
      </div>
    }</div>
  }
}

export default SettledCard;
