import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import { updateExpRecord } from "../../redux"

import "react-datepicker/dist/react-datepicker.css";

import { connect } from 'react-redux'
const mapStateToProps = state => {
  return {
    expRecord: state.expRecord,
    friends: state.friends,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    updateExpRecord: (data = false) => dispatch(updateExpRecord(data)),
  }
}

class AddExp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: '',
      widthId: "1",
      fname: this.props.friends[1].FName,
      disc: '',
      totalAmt: '',
      uShare: 0,
      uPerShare: 0,
      fShare: 0,
      fPerShare: 0,
      paidById: "0",
      calType: "0",
    }
  }
  handleWithList = (e) => {
    console.log(e.target.value)
    let fname = this.props.friends.filter(item => item.Fid == e.target.value)[0]
    this.setState({
      widthId: e.target.value,
      fname: fname.FName
    });
  }
  handleDisc = (e) => {
    this.setState({ disc: e.target.value });
  }
  handleAmt = (e) => {
    if (this.state.calType == 0) {
      this.setState({
        calType: 0,
        uShare: parseFloat(e.target.value) / 2,
        uPerShare: 50,
        fShare: parseFloat(e.target.value) / 2,
        fPerShare: 50,
      });
    } else if (this.state.calType == 2) {
      this.setState({
        calType: 1,
        uShare: 0,
        uPerShare: 0,
        fShare: parseFloat(e.target.value),
        fPerShare: 100,
      });
    } else if (this.state.calType == 1) {
      this.setState({
        calType: 2,
        uShare: parseFloat(e.target.value),
        uPerShare: 100,
        fShare: 0,
        fPerShare: 0,
      });
    }
    this.setState({ totalAmt: e.target.value });
  }
  handlePaidbyList = (e) => {
    this.setState({ paidById: e.target.value });
  }
  handleCalType = (e) => {
    if (e.target.value == 0 && parseFloat(this.state.totalAmt) > 0) {
      this.setState({
        calType: e.target.value,
        uShare: parseFloat(this.state.totalAmt) / 2,
        uPerShare: 50,
        fShare: parseFloat(this.state.totalAmt) / 2,
        fPerShare: 50,
      });
    } else if (e.target.value == 2 && parseFloat(this.state.totalAmt) > 0) {
      this.setState({
        calType: e.target.value,
        uShare: 0,
        uPerShare: 0,
        fShare: parseFloat(this.state.totalAmt),
        fPerShare: 100,
      });
    } else if (e.target.value == 1 && parseFloat(this.state.totalAmt) > 0) {
      this.setState({
        calType: e.target.value,
        uShare: parseFloat(this.state.totalAmt),
        uPerShare: 100,
        fShare: 0,
        fPerShare: 0,
      });
    } else {
      this.setState({ calType: e.target.value });
    }
  }
  handleChange = (e) => {
    this.setState({ date: e });
  }
  handleForm = (e) => {

  }
  handleSettleUp = (e) => {
    let onlydate = this.state.date.getDate();
    let month = this.state.date.getMonth() + 1;
    let year = this.state.date.getFullYear();

    let tempExpId = this.props.expRecord.length + 1;
    let tempDate = onlydate + '-' + month + '-' + year
    let tempTotalAmt = this.state.totalAmt
    let tempUserPaid = this.state.paidById == 0 ? true : false
    let tempUserShare = this.state.uShare
    let tempFriendShare = this.state.fShare
    let tempFriendName = this.state.fname

    let tempObj = {
      expId: tempExpId,
      date: tempDate,
      totalAmt: tempTotalAmt,
      userPaid: tempUserPaid,
      settled: false,
      userShare: tempUserShare,
      friendShare: tempFriendShare,
      friendName: tempFriendName,
      typeSettleUp: false
    }
    let newExpRecond = [...this.props.expRecord, tempObj]
    this.props.updateExpRecord(newExpRecond)
    this.setState({
      date: '',
      widthId: "1",
      fname: this.props.friends[1].FName,
      disc: '',
      totalAmt: '',
      uShare: 0,
      uPerShare: 0,
      fShare: 0,
      fPerShare: 0,
      paidById: "0",
      calType: "0",
    });
  }

  render() {
    const { widthId, disc, totalAmt, fname } = this.state
    const { friends } = this.props
    console.log(this.state)
    return <div>
      <button type="button" className="btn btn btn-primary bg-orange mx-2" data-bs-toggle="modal" data-bs-target="#add">Add an expense</button>
      <div className="modal fade" id="add" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header fill-green">
              <h5 className="modal-title" id="exampleModalLabel">Add expense</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <div className="d-flex mx-auto justify-content-center align-items-center">
                <select className="form-select form-select-sm w-25">
                  <option value="0">You</option>
                </select>
                <p className="px-4 my-0">and</p>
                <select className="form-select form-select-sm w-25" onChange={this.handleWithList} value={widthId}>
                  {friends.map((item, index) => index === 1 ? <option key={item.Fid} value={item.Fid}>{item.FName}</option> : <option key={item.Fid} value={item.Fid}>{item.FName}</option>)}
                </select>
              </div>
              <div className="d-flex my-4">
                <img src="https://s3.amazonaws.com/splitwise/uploads/category/icon/square_v2/uncategorized/general@2x.png" className="me-3" alt="" />
                <div className="d-flex flex-wrap align-content-between">
                  <div className="input-group w-100">
                    <input type="text" className="form-control" placeholder="Description" value={disc} onInput={this.handleDisc} />
                  </div>
                  <div className="input-group w-100">
                    <div className="input-group-prepend">
                      <span className="input-group-text">$</span>
                    </div>
                    <input type="number" className="form-control" placeholder="00" value={totalAmt} onInput={this.handleAmt} />
                    <div className="input-group-append">
                      <span className="input-group-text">.00</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="d-flex mx-auto justify-content-start align-items-center w-75">
                <p className="pe-3 my-0 text-start">Paid by</p>
                <select className="form-select form-select-sm w-25" onChange={this.handlePaidbyList} value={this.state.paidById}>
                  <option value="0">You</option>
                  <option value={widthId}>{fname}</option>
                </select>
                <p className="px-3 my-0">and split</p>
                <select className="form-select form-select-sm w-25 flex-grow-1" onChange={this.handleCalType} value={this.state.calType}>
                  <option value="0">Equally</option>
                  <option value="1">{'You own ' + fname}</option>
                  <option value="2">{fname + ' owns you'} </option>
                  <option value="3">Custom</option>
                </select>
              </div>
              <div className="input-group my-3 w-50 mx-auto">
                <input type="text" className="form-control" value={this.state.uShare} onInput={this.handleForm} />
                <span className="input-group-text">$</span>
                <input type="text" className="form-control" value={this.state.fShare} onInput={this.handleForm} />
              </div>
              <div className="input-group mb-3 w-50 mx-auto">
                <input type="text" className="form-control" value={this.state.uPerShare} onInput={this.handleForm} />
                <span className="input-group-text">%</span>
                <input type="text" className="form-control" value={this.state.fPerShare} onInput={this.handleForm} />
              </div>
              <DatePicker selected={this.state.date} dateFormat="dd-MM-yyyy" onChange={this.handleChange} />
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary btn-primary-dark" data-bs-dismiss="modal" onClick={this.handleSettleUp}>Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddExp);
