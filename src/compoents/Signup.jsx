import React, { Component } from 'react';
import { connect } from 'react-redux'
import { addUserData } from '../redux'
import validator from 'validator';
import { Link } from 'react-router-dom';

import Header from './landingComponents/Header'
import Footer from './landingComponents/Footer'

const mapStateToProps = state => {
  return {
    userData: state.userData,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addUserData: (data) => dispatch(addUserData(data)),
  }
}


class Signup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Name: '',
      Email: '',
      Password: '',
      isError: false,
      nameInValid: false,
      emailInValid: false,
      passInValid: false
    }
  }
  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  validateForm = () => {
    const { Name, Email, Password } = this.state;
    let isError = false;
    this.setState({
      isError: false,
      nameInValid: false,
      emailInValid: false,
      passInValid: false
    })
    if (validator.isEmpty(Name)) {
      isError = true
      this.setState({
        isError: true,
        nameInValid: 'Name can not be empty'
      })
    } else if (validator.isAlpha(Name, 'en-US') === false) {
      isError = true
      this.setState({
        isError: true,
        nameInValid: 'Name can not be invalid value'

      })
    }

    if (validator.isEmpty(Email)) {
      isError = true
      this.setState({
        isError: true,
        emailInValid: 'Email can not be empty'
      })
    } else if (validator.isEmail(Email, { blacklisted_chars: ' ' }) === false) {
      isError = true
      this.setState({
        isError: true,
        emailInValid: 'Invalid mail'
      })
    }
    let checkLIst = {
      minLength: 8, minLowercase: 1,
      minUppercase: 1, minNumbers: 1
    }

    if (validator.isEmpty(Password)) {
      isError = true
      this.setState({
        isError: true,
        passInValid: 'Password field can not be empty'
      })
    } else if (validator.isStrongPassword(Password, checkLIst) === false) {
      isError = true
      this.setState({
        isError: true,
        passInValid: `Need 1 lowercase, 1 uppercase alphabet and 2 number`
      })
    }

    return isError
  }
  submitHandler = e => {
    e.preventDefault()
    let isError = this.validateForm();
    console.log('I am after validator', this.state.isError, isError)
    if (isError !== true) {
      let userdata = {
        ...this.state,
        isError: false,
        nameInValid: false,
        emailInValid: false,
        passInValid: false
      }
      this.props.addUserData(userdata)
      this.setState({
        Name: '',
        Email: '',
        Password: '',
      })

    }
  }
  render() {
    const { Name, Email, Password, passInValid, nameInValid, emailInValid, isError } = this.state;
    return <div>
      <Header />
      <div className="d-flex align-items-center container-fluid bg-abstract vh-75 text-start">
        <div className="w-75 mx-auto my-5">
          <div className="container w-50 p-5 bg-white login-card">
            <h2>Signup</h2>
            <form onSubmit={this.submitHandler} autoComplete="new-password" >
              <div className="mb-3">
                <label className="form-label">Name</label>
                <input type="text" name="Name" value={Name} className="form-control" id="Name" aria-describedby="emailHelp" onChange={this.changeHandler} placeholder="Enter Name" />
                <p className="text-danger text-center mt-1">
                  {isError ? nameInValid : undefined}
                </p>
              </div>
              <div className="mb-3">
                <label className="form-label">Email address</label>
                <input type="email" name="Email" value={Email} className="form-control" id="Email" aria-describedby="emailHelp" onChange={this.changeHandler} placeholder="Enter email" />
                <p className="text-danger text-center mt-1">
                  {isError ? emailInValid : undefined}
                </p>
              </div>
              <div className="mb-3">
                <label className="form-label">Password</label>
                <input type="password" name="Password" value={Password} className="form-control" id="Password" onChange={this.changeHandler} placeholder="Enter password" autoComplete="new-password" />
                <p className="text-danger text-center mt-1">
                  {isError ? passInValid : undefined}
                </p>
              </div>
              <div className="mb-3 form-check">
                <a target="_blank" href="/terms">By signing up, you accept the Splitwise Terms of Service.</a>
              </div>
              <button type="submit" className="btn btn btn-success btn-primary-dark my-2 px-5 w-100">Sign Me Up</button>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
