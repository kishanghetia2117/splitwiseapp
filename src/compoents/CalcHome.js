import React, { Component } from 'react';

import Header from './landingComponents/Header'
import Footer from './landingComponents/Footer'
import ExpSummary from './calAppComponents/ExpSummary';
import ExpData from './calAppComponents/ExpData'
import { connect } from 'react-redux'
import { AddFriend } from '../redux'

const mapStateToProps = state => {
  return {
    friends: state.friends,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    AddFriend: (data) => dispatch(AddFriend(data)),
  }
}

class CalcHome extends Component {
  constructor(props) {
    super(props)

    this.state = {
      DashBord: true,
      expenses: false,
      friednName: '',
    }
  }

  showDashboard = () => {
    this.setState({
      DashBord: true,
      expenses: false,
    });
  }
  showAllExpenses = () => {
    this.setState({
      DashBord: false,
      expenses: true,
    });
  }
  handleFriendInput = (e) => {
    this.setState({ friednName: e.target.value });
  }
  handleAddFriend = () => {
    console.log(this.state.friednName)
    let tempObj = {
      FName: this.state.friednName,
      Fid: this.props.friends.length
    }
    let tempFriend = [...this.props.friends, tempObj]
    this.setState({ friednName: '' });
    this.props.AddFriend(tempFriend)
  }
  render() {
    const { DashBord, expenses } = this.state
    return <div>
      <Header />
      <div className="w-100 vh-100 d-flex justify-content-center">
        <div className="w-50  h-100">
          <div className="row h-100">
            <div className="col-sm-2 flex-column p-0 text-start  pt-2">
              <button type="button" onClick={this.showDashboard} className={DashBord ? 'btn btn-light w-100 mb-1 text-start active-cal-bord btn-dash' : "btn btn-light w-100 mb-1 text-start btn-dash"}>
                <img className="w-10per me-2 " src="https://img.icons8.com/external-vitaliy-gorbachev-lineal-vitaly-gorbachev/60/000000/external-dashboard-blogger-vitaliy-gorbachev-lineal-vitaly-gorbachev.png" alt="" />
                DashBord</button>
              <button type="button" onClick={this.showAllExpenses} className={expenses ? 'btn btn-light w-100 mb-1 text-start active-cal-bord btn-dash' : "btn btn-light w-100 mb-1 text-start btn-dash"} >
                <img className="w-10per me-2" src="https://img.icons8.com/external-kmg-design-detailed-outline-kmg-design/64/000000/external-dashboard-user-interface-kmg-design-detailed-outline-kmg-design.png" alt="" />
                All expenses</button>
              <div className="mt-3">
                <p className='d-flex flex-row justify-content-between align-items-center ps-2'>
                  Friends
                  <span className='pe-3 onhoverADD' data-bs-toggle="modal" data-bs-target="#addFriend" >+ add</span>
                </p>
                <div className="modal fade" id="addFriend" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-header fill-green">
                        <h5 className="modal-title" id="exampleModalLabel">Add friend</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div className="modal-body">
                        <form>
                          <div className="form-group">
                            <input type="email" className="form-control" placeholder="Add Friend" onInput={this.handleFriendInput} />
                          </div>
                        </form>
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary btn-primary-dark" data-bs-dismiss="modal" onClick={this.handleAddFriend}>Add </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  {this.props.friends.map(item => <p className='ps-2 mb-1 bg-light'>
                    <svg className='me-2 bi bi-person-fill ' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                      <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                    </svg>
                    {item.FName}
                  </p>)}
                </div>
              </div>
            </div>
            <ExpData dahsProp={DashBord} />
            <ExpSummary />
          </div>
        </div>
      </div>
      {/* <SplitWise /> */}
      <Footer />
    </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalcHome);
