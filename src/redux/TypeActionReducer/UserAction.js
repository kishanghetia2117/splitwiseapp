import { USER_DATA, USER_LOGIN, USER_OUT, UPDATE_REC, ADD_FRIEND } from './actionTypes'

export const addUserData = (data) => {
  return {
    type: USER_DATA,
    payload: data
  }
}

export const userlogIn = (isLogin) => {
  return {
    type: USER_LOGIN,
    payload: isLogin
  }
}

export const userLogOut = (isLogin) => {
  return {
    type: USER_OUT,
    payload: isLogin
  }
}
export const updateExpRecord = (rec) => {
  return {
    type: UPDATE_REC,
    payload: rec
  }
}

export const AddFriend = (friend) => {
  return {
    type: ADD_FRIEND,
    payload: friend
  }
}