import { USER_DATA, USER_LOGIN, USER_OUT, UPDATE_REC, ADD_FRIEND } from './actionTypes'

const initialState = {
  userData: {
    Fid: 0,
    Email: "kishanghetia2117@gmail.com",
    Name: "Kishan",
    Password: "Abcd@123",
    emailInValid: false,
    isError: false,
    nameInValid: false,
    passInValid: false,
  },
  login: false,
  friends: [
    {
      FName: "You",
      Fid: "0",
    },
    {
      FName: "Anil",
      Fid: "1"
    },
    {
      FName: "Asim",
      Fid: "2"
    }
  ],
  expRecord: [
    {
      expId: 1,
      date: "02-12-2021",
      totalAmt: 200,
      userPaid: false,
      settled: true,
      description: "Trip",
      userShare: 150,
      friendShare: 50,
      friendName: "Asim",
      typeSettleUp: false
    },
    {
      expId: 2,
      date: "07-02-2021",
      totalAmt: 650,
      userPaid: true,
      settled: false,
      description: "Game Day",
      userShare: 300,
      friendShare: 350,
      friendName: "Anil",
      typeSettleUp: false
    },
    {
      expId: 6,
      date: "09-02-2021",
      totalAmt: 100,
      userPaid: false,
      settled: false,
      description: "Rent",
      userShare: 0,
      friendShare: 100,
      friendName: "Vinod",
      typeSettleUp: true
    },
    {
      expId: 3,
      date: "07-02-2021",
      totalAmt: 700,
      userPaid: true,
      settled: false,
      description: "Car fuel",
      userShare: 350,
      friendShare: 350,
      friendName: "Yash",
      typeSettleUp: false
    },
    {
      expId: 5,
      date: "09-02-2021",
      totalAmt: 300,
      userPaid: true,
      settled: false,
      description: "Hotel Food",
      userShare: 150,
      friendShare: 150,
      friendName: "Vinod",
      typeSettleUp: true
    },
    {
      expId: 4,
      date: "09-02-2021",
      totalAmt: 500,
      userPaid: false,
      settled: false,
      description: "Card payment",
      userShare: 250,
      friendShare: 250,
      typeSettleUp: false,
      friendName: "Varun",
    },

  ]
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        userData: action.payload
      };
    case USER_LOGIN:
      return {
        ...state,
        login: action.payload
      };
    case USER_OUT:
      return {
        ...state,
        login: action.payload
      };
    case UPDATE_REC:
      return {
        ...state,
        expRecord: action.payload
      };
    case ADD_FRIEND:
      return {
        ...state,
        friends: action.payload
      };
    default:
      return state
  }
}

export default userReducer